# Ways to implement FFT in Grafana

## Fast Fourier Transformation - Background

- A fast Fourier transform (FFT) is an algorithm that computes the discrete Fourier transform (DFT) of a sequence, or its inverse (IDFT). 

- The Fourier Transformation is a mathematical function capable of transforming a periodic signal from the time domain to the frequency domain. Each periodic signal can be obtained as the weighted sum of periodic sinusoidal signals at different frequencies. 

- The FFT (Fast Fourier Transform) is a very efficient DFT implementation, which is largely employed in the modern computer systems. Applications extend beyond sound processing and include radio frequency and image processing.


![FFT](https://cdn-learn.adafruit.com/assets/assets/000/011/430/medium800/microcontrollers_fft_example.png?1380761784)


#### Input

<br>The input to the FFT node is a continuous stream of finite time sequences of samples, the number of samples in each batch must be the same (controlled by upstream window node), each sample reading must be taken with the same cadence. A typical input can be generated as follow:

- sensor -> periodic sampling -> continuous streaming -> windowing --> FFT --> output node.

<br>The frequency range is subdivided in a number of equally spaced frequency bands (defined by the user parameter). The FFT Node is designed to process a batch of samples each second, then it records the aggregate frequency domain magnitude per each frequency band. The maximum number of frequency bands that the user can select is equal to the number of samples per second divided by 2.

#### Output

The output is the data set to create a histogram, that records per each second the spectrum of the signal divided in the given number of frequency bands. 
The FFT node produces one record per each batch. Each record contains the timestamp of the last sample of the batch, and as many fields as the frequency bands chosen by the user, these fields contain the magnitude associated to that frequency band. Each frequency band field takes the name of the lower frequency in the band.

- The FFT output timestamp is the timestamp of the last sample of the batch, it is to be considered the batch timestamp.

- The FFT output record contains as many frequency band fields as the buckets in which the FFT output magnitude data are aggregated.

- Each frequency band field name is the lower frequency of the frequency band included between the given value and the next.

- The magnitude value is the sum of the individual magnitude of all the frequency components calculated by the FFT algorithm belonging to the given frequency band.
## Implementation of FFT
#### Using Plot.ly plugin 

The Plot.ly plugin in Grafana can be extended to compute arbitrary transformations of the time series data such as FFT.

![Plot.ly](https://raw.githubusercontent.com/guanyilun/grafana-plotly-panel/master/src/img/screenshot-fft.png)

<b>Installation</b>

For local instances, plugins are installed and updated via a simple CLI command.

Use the grafana-cli tool to install Plotly from the commandline:

<pre><code>grafana-cli plugins install natel-plotly-panel</code></pre>

#### Basics 

Plotly plugin plugin is one of the best choice to plot non-time series data. 

<br>It should be started by removing all filtering by time from the query. Then only add one column with a timestamp. Then we should filter by either last FFT data or some other metric.
We can use Telegraf to parse the JSON messages and then forward them to InfluxDB. In Grafana, this would give separate columns for each JSON fields and the arrays of the data gets splited as well. 
 
- Since, FFT is not “time-variant” to display it as time-series, so it would be nice if it could be visualized as a kind of X-Y plot, with X being the Frequencies and Y the respective magnitudes, and these would be refreshed each time a new message arrives in Grafana. Write the query such that we get one column with frequencies and one column with magnitude for the selected time range. Just use something like last() to only show the most recent analysis. 

![axis](https://aws1.discourse-cdn.com/standard14/uploads/grafana/optimized/2X/c/ce91f4e87be59954834979a239beab868f94fec4_2_690x148.png)

- If the amount of the data collected is small then the number could be replaced with a variable or lese change the data structure so you have a field for the id instead. 

- We can use the time series buckets mode in the axis settings. It should display one field for each frequency with the color showing the magnitude. But the only problem would be that it uses the column names for the y axis. We could use “as” to rename the columns in the query, although it may not be done automatically. Perhaps we could change the data recording code to replace it with something else. That would show time on x, frequency on y and magnitude in the color.

![](https://aws1.discourse-cdn.com/standard14/uploads/grafana/optimized/2X/7/70540d67b87c7759f5402cae3fcc16a74f1cf423_2_690x358.png)

- An alternative could be the plotly panel, which allows to create custom traces with custom x and y values. So we could select xaxis: time, yaxis: frequency and metric: magnitude. It only displays points. It allows log scale for y and for the data color.

![](https://aws1.discourse-cdn.com/standard14/uploads/grafana/optimized/2X/e/e7034a6b0e15ac05c300ef31f2188e4df69fcb26_2_690x371.png)

- Regarding the scale of the buckets they are always evenly spaced. The data could be saved in a logarithmic scale, when collecting it to have columns already spaced correctly.
