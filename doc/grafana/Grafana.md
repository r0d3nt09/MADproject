# Introduction to Grafana 
- The analytics platform for all metrics.
- Allows to query, visualize, alert on and understand your metrics irrespective of where they are stored. 
- We can Create, explore, and share dashboards and foster a data driven culture.
- An open-source, nightly built dashboarding, analytics, and monitoring platform that is tinkered for connection with a variety of sources like Elasticsearch, Influxdb, Graphite, Prometheus, AWS Cloud Watch, and many others.
- Ability to bring several data sources together in one dashboard with adding rows that will host individual panels (each with visual type).

# Pros
- Nice design of the visuals and interface;
- Comfortable to work on several sources;
- Easy to connect to the sources.

# Cons
- Takes time to figure out where filters go and how to compose certain visuals;
- Abstraction of unnecessary data(sending raw queries to the data source from Grafana);
- Plugin installation — default install has only a few visual types, and we end up installing plugins (even for pie chart). So,Grafana may have few problems during installing plugins, which are not easy to debug.

# Features of Grafana

## Visualize
![Visualize](https://grafana.com/static/img/grafana/feature-visualize.png)

- From heatmaps to histograms. Graphs to geomaps. Grafana has a plethora of visualization options to understand data.
- Fast and flexible visualizations with a multitude of options us to visualize data.

## Dynamic Dashboards
![Grafana](https://www.8bitmen.com/wp-content/uploads/2019/01/grafana-min-1024x512.png)

- The dashboards contain a gamut of visualization options such as geo maps, heat maps, histograms, all the variety of charts & graphs which a business typically requires to study data.
- A dashboard contains several different individual panels on the grid. Each panel has different functionalities.
- The dashboards pull data from the plugged-in data sources such as Graphite, Prometheus, Influx DB, ElasticSearch, MySQL, PostgreSQL etc.These are a few of many data sources which Grafana supports by default.
- We can Create dynamic & reusable dashboards with template variables that appear as dropdowns at the top of the dashboard.

## Explore Metrics
- Explore data through ad-hoc queries and dynamic drilldown.
- Split view and compare different time ranges, queries and data sources side by side.

## Explore Logs
- We can switch from metrics to logs with preserved label filters. 
- We can Quickly search through all your logs or streaming them live.
- Works best with Loki data source(Loki is a horizontally-scalable, highly-available, multi-tenant log aggregation system. It is designed to be very cost effective and easy to operate. It does not index the contents of the logs, but rather a set of labels for each log stream.)

## Alert
![Alert](https://grafana.com/static/img/grafana/feature-alert.svg)

- Define thresholds visually, and get notified via Slack, PagerDuty, and more.
- Visually define alert rules for most important metrics.
- Grafana will continuously evaluate and send notifications to systems like Slack, PagerDuty, VictorOps, OpsGenie.

## Unify
![Unify](https://grafana.com/static/img/grafana/feature-unify.svg)
- Bring data together to get better context as it supports dozens of databases and Mix them together in the same Dashboard as Mixed Data Sources in the same graph.
- We can specify a data source on a per-query basis. This works for even custom datasources.

## Annotations
- Annotate graphs with rich events from different data sources. Hover over events shows you the full event metadata and tags.

## Ad-hoc Filters
- Allow you to create new key/value filters on the fly, which are automatically applied to all queries that use that data source.

## Open
![Open](https://grafana.com/static/img/grafana/feature-open.svg)

- It’s completely open source, and backed by a vibrant community. Use Grafana Cloud, or easily install on any platform.

## Extend
![Extend](https://grafana.com/static/img/grafana/feature-extend.svg)

- We can Discover hundreds of dashboards and plugins in the official library. 

## Collaborate
![Collaborate](https://grafana.com/static/img/grafana/feature-collaborate.png)

- We can share data and dashboards across teams as it empowers users, and helps foster a data driven culture.

## Built-in Graphite Support
![BLABLA](https://grafana.com/static/img/grafana/graphite_query_editor.png)

- Grafana includes a built in Graphite query parser that takes writing graphite metric expressions to adavanced level. 
- Expression are easier to edit.

# Install Grafana on Ubuntu 20.04/18.04

<br>Two ways to Install Grafana:
<br>**1.Using the official APT repository**
<br>**2.Installing from .deb package**
<br>The preferred method is using apt repository as you can easily upgrade to the latest release using the apt package manager for Ubuntu.

- **Step 1: Update system**
<br>Ubuntu system should be up to date.
<br>`sudo apt update`

- **Step 2: Add Grafana 6 APT repository**
<br>Add Grafana gpg key which allows you to install signed packages.
<br> `sudo apt-get install -y gnupg2 curl  software-properties-common`
<br> ` curl https://packages.grafana.com/gpg.key | sudo apt-key add `
<br>install Grafana APT repository:
<br>`sudo add-apt-repository "deb https://packages.grafana.com/oss/deb stable main"`

- **Step 3: Install Grafana**
<br>1.Once the repository is added, proceed to update Apt repositories and install Grafana
<br>`sudo apt-get update`
<br>`sudo apt-get -y install grafana`
<br>2.Start Grafana service.
<br>`sudo systemctl enable --now grafana-server`
<br>`systemctl status grafana-server.service`

- **Step 4: Open Port on Firewall (Optional)**
<br>Grafana default http port is 3000, allow access to this port on the firewall(Ubuntu comes with ufw firewall)

- **Step 5: Access Grafana Dashboard on Ubuntu** 
![login](https://computingforgeeks.com/wp-content/uploads/2018/06/grafana-login.png?ezimgfmt=ng:webp/ngcb8)
<br>1.Access Grafana Dashboard using the server IP address or hostname and port 3000.
<br>**Default logins are:**
<br>**Username: admin**
<br>**Password: admin**
<br>2.Change Admin Password
![login1](https://computingforgeeks.com/wp-content/uploads/2018/06/grafana-change-password.png?ezimgfmt=ng:webp/ngcb8)
<br>Change admin password from default admin. Login and navigate to:
<br>Preferences > Change Password

# Installing Grafana on Linux 
First, add rpm repo:
<br> **Bash:** 
<br>` sudo cat > /etc/yum.repos.d/grafana.repo <<- repoconf `
<br>`[grafana]`
<br>`name=grafana`
<br>`baseurl=https://packagecloud.io/grafana/stable/el/6/$basearch`
<br>`repo_gpgcheck=1`
<br>`enabled=1`
<br>`gpgcheck=1`
<br>`gpgkey=https://packagecloud.io/gpg.key https://grafanarel.s3.amazonaws.com/RPM-GPG-KEY-grafana`
<br>`sslverify=1`
<br>`sslcacert=/etc/pki/tls/certs/ca-bundle.crt`
<br>`repoconf`

<br>**Then, install using yum:**
<br>**Bash:**
<br>`sudo yum install grafana`
<br>Installing via repo adds system unit for running in daemon mode.
<br>**To run as daemon, do the following:**
<br>**Bash:**
<br>`systemctl daemon-reload`
<br>`systemctl start grafana-server`
<br>`systemctl status grafana-server`
<br>**If you want to enable auto-startup on the boot, run:**
<br>**Bash:**
<br>`sudo systemctl enable grafana-server.service`

## Configuration
- All defaults for running are configured in environment variables in /etc/sysconfig/grafana-server:
<br>`GRAFANA_USER=grafana`
<br>`GRAFANA_GROUP=grafana`
<br>`GRAFANA_HOME=/usr/share/grafana`
<br>`LOG_DIR=/var/log/grafana`
<br>`DATA_DIR=/var/lib/grafana`
<br>`MAX_OPEN_FILES=10000`
<br>`CONF_DIR=/etc/grafana`
<br>`CONF_FILE=/etc/grafana/grafana.ini`
<br>`RESTART_ON_UPGRADE=true`
<br>`PLUGINS_DIR=/var/lib/grafana/plugins`

<br>As to Grafana configurations, everything is listed (including defaults) in ` /etc/grafana/grafana.ini.`
- If we want to access Grafana from outside (not localhost only) set `http_addr config` to bind to all interfaces explicitly or leave it blank to do the same thing implicitly.
- If Grafana is still inaccessible, make sure that the firewall does not block traffic on Grafana’s port.
- To add a port to allowed, use:
<br>**Bash:**
<br>`GRAFANA_USER=grafanafirewall-cmd — zone=public — add-port=3000/tcp — permanent`
<br>`firewall-cmd — reload`
- Then, check it in:
<br>**Bash:**
<br>`iptables-save | grep 3000`

# Installing plugins
- Installation of plugins may cause several troubles due to incompatibilities of Grafana versions. The most common problem is that the plugin is installed but not detected and thus not usable. 
- To avoid the above problem,First, make sure that `/var/lib/grafana/` folder is owned by Grafana user and has all permissions. If not, then run:
<br>**Bash:**
<br>`sudo chown -R grafana /var/lib/grafana/`
<br>`sudo chmod -R 700 /var/lib/grafana/`
<br>Stop Grafana:
<br>**Bash:**
<br>`systemctl stop grafana-server`
- Make sure to clear cache in the browser, from which you access Grafana.
- Then, install a plugin using the `cli utility`:
<br>**Bash:**
<br>`grafana-cli plugins install`
- Check that your plugin installed successfully:
<br>**Bash:**
<br>`grafana-cli plugins ls`
- After that, start Grafana up again:
<br>**Bash:**
<br>`systemctl stop grafana-server`
- Check your installation in a browser.

# Building  Dashboard
- log in. By default, Grafana creates an admin user with admin password on startup (maybe changed in /etc/grafana/grafana.ini)
![G login](https://miro.medium.com/max/1000/0*x2EYDx4V9Fk6u7oB.png)
- After log in, we will be prompted to connect to first data source.
![Home dashboard](https://miro.medium.com/max/1000/0*siHDlTq7Rzsruvyf.png)
- Connect to an instance of the elasticsearch cluster:
![Getting started](https://miro.medium.com/max/1000/0*fq0jA6RhZ-TP1BsR.png)
- Fill all the fields to connect to the cluster. Note that in place of the index name you may specify the pattern for lookup.
- Then proceed to create the dashboard.
![h](https://miro.medium.com/max/1000/0*lbXa3uGqvI_pD4Y6.png)
- We will get an empty dashboard with one row. In that row we can put any panels and each panel is responsible for one visual.
![D](https://miro.medium.com/max/1000/0*lAfC46dKv52h-6st.png)
- Choose the type of panel to interact with it.
- In the upper left corner choose the time interval for which the data will be used and displayed (in our case we set up the time field to be the one from data itself;use logstash @timestamp).
![j](https://miro.medium.com/max/1000/0*7qQcyNaqBhs9V_bA.png)
- The tabs of the Graph are grouping the settings logically to discover. 
- Data is queried for building visual is all mirrored in Metrics tab.
- The visual looks is set in Display tab.
- Save the dashboard by clicking diskette sign on the top and giving it concise name.


